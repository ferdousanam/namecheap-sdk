## Namecheap Laravel SDK

### Installation

#### Add to `composer.json`

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/ferdousanam/namecheap-sdk.git"
    }
  ]
}
```

#### Run the composer command to install

```shell
composer require ferdousanam/namecheap
```

#### Update the dependency if there is a newer version (_optional_)

```shell
composer update ferdousanam/namecheap
```

### Configuration

#### Add credentials to `.env` file

```dotenv
NAMECHEAP_API_DOMAIN=https://api.namecheap.com
NAMECHEAP_USER=
NAMECHEAP_API_KEY=
NAMECHEAP_CLIENT_IP=
```

#### Add namecheap coupon to `.env` file (_optional_)

```dotenv
NAMECHEAP_COUPON=DMNS2021
```

### Namecheap API Documentation

- [Introduction](https://www.namecheap.com/support/api)
- [Methods](https://www.namecheap.com/support/api/methods)

## Author

Contact Author if interested for author as author is too lazy to write documentation
🙁 [Ferdous Anam](https://ferdousanam.gitlab.io).
