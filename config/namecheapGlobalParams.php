<?php

return [
    'api_domain' => env('NAMECHEAP_API_DOMAIN'),
    'username' => env('NAMECHEAP_USER'),
    'api_user' => env('NAMECHEAP_USER'),
    'api_key' => env('NAMECHEAP_API_KEY'),
    'client_ip' => env('NAMECHEAP_CLIENT_IP'),
    'coupon_code' => env('NAMECHEAP_COUPON'),
];
