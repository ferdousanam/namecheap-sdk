<?php

namespace Anam\Namecheap;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;

abstract class NamecheapAbstract
{
    protected $client;
    protected $couponCode;
    protected $globalParameters;

    public function __construct()
    {
        $this->couponCode = App::make('config')->get('namecheapGlobalParams.coupon_code');

        $this->globalParameters = [
            'UserName' => App::make('config')->get('namecheapGlobalParams.username'),
            'ApiUser' => App::make('config')->get('namecheapGlobalParams.api_user'),
            'ApiKey' => App::make('config')->get('namecheapGlobalParams.api_key'),
            'ClientIp' => App::make('config')->get('namecheapGlobalParams.client_ip'),
        ];

        $this->client = new Client(['base_uri' => App::make('config')->get('namecheapGlobalParams.api_domain')]);
    }

    protected function toJson($request)
    {
        $xml_string = $request->getBody()->getContents();
        $xml = simplexml_load_string($xml_string);
        return json_encode($xml);
    }

    protected function toArray($request): array
    {
        return json_decode($this->toJson($request), TRUE);
    }
}
