<?php


namespace Anam\Namecheap;

use Illuminate\Support\ServiceProvider;

class NamecheapServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/namecheapGlobalParams.php';
        $this->mergeConfigFrom($configPath, 'namecheapGlobalParams');

        $configPath = __DIR__ . '/../config/namecheap.php';
        $this->mergeConfigFrom($configPath, 'namecheap');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__ . '/../config/namecheap.php';
        $this->publishes([
            $configPath => $this->app->configPath('namecheap.php')
        ], 'namecheap-config');
    }
}
