<?php

namespace Anam\Namecheap;

class DomainDns extends NamecheapAbstract
{
    /**
     * Sets domain to use our default DNS servers.
     * Required for free services like Host record management, URL forwarding, email forwarding, dynamic dns and other value added services.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/set-default
     *
     * @param string $SLD
     * @param string $TLD
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setDefault($SLD, $TLD)
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.setDefault',
                'SLD' => $SLD,
                'TLD' => $TLD,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Sets domain to use custom DNS servers.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/set-custom
     *
     * @param string $SLD
     * @param string $TLD
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setCustom($SLD, $TLD)
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.setCustom',
                'SLD' => $SLD,
                'TLD' => $TLD,
                'NameServers' => implode(',', [
                    'ns1.digitalocean.com',
                    'ns2.digitalocean.com',
                    'ns3.digitalocean.com',
                ]),
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Gets a list of DNS servers associated with the requested domain.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/get-list
     *
     * @param string $SLD
     * @param string $TLD
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getList($SLD, $TLD)
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.getList',
                'SLD' => $SLD,
                'TLD' => $TLD,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Retrieves DNS host record settings for the requested domain.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/get-hosts
     *
     * @param string $SLD
     * @param string $TLD
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHosts($SLD, $TLD)
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.getHosts',
                'SLD' => $SLD,
                'TLD' => $TLD,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Gets email forwarding settings for the requested domain.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/get-email-forwarding
     *
     * @param string $domainName
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getEmailForwarding($domainName)
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.getEmailForwarding',
                'DomainName' => $domainName,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Sets email forwarding for a domain name.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/set-email-forwarding
     *
     * @param string $domainName
     * @param array $config
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setEmailForwarding($domainName, $config = [])
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.getEmailForwarding',
                'DomainName' => $domainName,
            ], $config)
        ]);
        return $this->toArray($request);
    }

    /**
     * Sets DNS host records settings for the requested domain.
     * @link https://www.namecheap.com/support/api/methods/domains-dns/set-hosts
     *
     * @param string $SLD
     * @param string $TLD
     * @param array $config
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setHosts($SLD, $TLD, $config = [])
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.dns.setHosts',
                'SLD' => $SLD,
                'TLD' => $TLD,
            ], $config)
        ]);
        return $this->toArray($request);
    }
}
