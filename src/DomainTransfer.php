<?php

namespace Anam\Namecheap;

class DomainTransfer extends NamecheapAbstract
{
    public function getList($listType = 'ALL', $searchTerm = null, $page = 1, $pageSize = 20, $sortBy = null)
    {
        $data = [
            'ListType' => $listType,
            'Page' => $page,
            'PageSize' => $pageSize,
        ];
        if ($searchTerm) {
            $data['SearchTerm'] = $searchTerm;
        }
        if ($sortBy) {
            $data['SortBy'] = $sortBy;
        }

        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.transfer.getList',
            ], $data)
        ]);
        return $this->toArray($request);
    }
}
