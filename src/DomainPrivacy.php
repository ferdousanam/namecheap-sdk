<?php

namespace Anam\Namecheap;

class DomainPrivacy extends NamecheapAbstract
{
    /**
     * Enables domain privacy protection for the WhoisguardID.
     *
     * @param int $WhoisguardID
     * @param string $ForwardedToEmail
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function enable(int $WhoisguardID, string $ForwardedToEmail): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.whoisguard.enable',
                'WhoisguardID' => $WhoisguardID,
                'ForwardedToEmail' => $ForwardedToEmail,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Disables domain privacy protection for the WhoisguardID.
     *
     * @param int $WhoisguardID
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function disable(int $WhoisguardID): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.whoisguard.disable',
                'WhoisguardID' => $WhoisguardID,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Gets the list of domain privacy protection.
     *
     * @param string $ListType
     * @param int $Page
     * @param int $PageSize
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getList(string $ListType = 'ALL', int $Page = 1, int $PageSize = 2): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.whoisguard.getList',
                'ListType' => $ListType,
                'Page' => $Page,
                'PageSize' => $PageSize,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Renews domain privacy protection.
     *
     * @param string $WhoisguardID
     * @param int $Years
     * @param int|null $PromotionCode
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function renew(string $WhoisguardID, int $Years = 1, int $PromotionCode = null): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.whoisguard.renew',
                'WhoisguardID' => $WhoisguardID,
                'Years' => $Years,
                'PromotionCode' => $PromotionCode,
            ])
        ]);
        return $this->toArray($request);
    }
}
