<?php

namespace Anam\Namecheap;

class User extends NamecheapAbstract
{
    public function getPricing(string $productType = 'DOMAIN', string $actionName = 'REGISTER', string $productCategory = 'DOMAINS', string $productName = 'COM')
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.users.getPricing',
                'ProductType' => $productType,
                'ProductCategory' => $productCategory,
                'ActionName' => $actionName,
                'ProductName' => $productName,
            ])
        ]);
        return $this->toArray($request);
    }

    public function getBalances()
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.users.getBalances',
            ])
        ]);
        return $this->toArray($request);
    }

    public function changePassword(string $oldPassword, string $newPassword): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.users.changepassword',
                'OldPassword' => $oldPassword,
                'NewPassword' => $newPassword,
            ])
        ]);
        return $this->toArray($request);
    }
}
