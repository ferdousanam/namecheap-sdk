<?php

namespace Anam\Namecheap;

use Illuminate\Support\Facades\App;

class DomainNs extends NamecheapAbstract
{
    /**
     * Creates a new nameserver.
     *
     * @param string $SLD
     * @param string $TLD
     * @param string $nameserver
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(string $SLD, string $TLD, string $nameserver = 'ns1.domain.com'): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.ns.create',
                'SLD' => $SLD,
                'TLD' => $TLD,
                'Nameserver' => $nameserver,
                'IP' => App::make('config')->get('namecheap.server_ip'),
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Deletes a nameserver.
     *
     * @param string $SLD
     * @param string $TLD
     * @param string $nameserver
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $SLD, string $TLD, string $nameserver = 'ns1.domain.com'): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.ns.create',
                'SLD' => $SLD,
                'TLD' => $TLD,
                'Nameserver' => $nameserver,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Retrieves information about a registered nameserver.
     *
     * @param string $SLD
     * @param string $TLD
     * @param string $nameserver
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getInfo(string $SLD, string $TLD, string $nameserver = 'ns1.domain.com'): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.ns.getInfo',
                'SLD' => $SLD,
                'TLD' => $TLD,
                'Nameserver' => $nameserver,
            ])
        ]);
        return $this->toArray($request);
    }

    /**
     * Updates the IP address of a registered nameserver.
     *
     * @param string $SLD
     * @param string $TLD
     * @param string $nameserver
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(string $SLD, string $TLD, string $nameserver = 'ns1.domain.com'): array
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.ns.getInfo',
                'SLD' => $SLD,
                'TLD' => $TLD,
                'Nameserver' => $nameserver,
                'OldIP' => App::make('config')->get('namecheap.server_ip'),
                'IP' => App::make('config')->get('namecheap.server_ip'),
            ])
        ]);
        return $this->toArray($request);
    }
}
