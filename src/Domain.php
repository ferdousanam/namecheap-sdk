<?php

namespace Anam\Namecheap;

use Illuminate\Support\Facades\App;

class Domain extends NamecheapAbstract
{
    public function getList($listType = 'ALL', $searchTerm = null, $page = 1, $pageSize = 20, $sortBy = null)
    {
        $data = [
            'ListType' => $listType,
            'Page' => $page,
            'PageSize' => $pageSize,
        ];
        if ($searchTerm) {
            $data['SearchTerm'] = $searchTerm;
        }
        if ($sortBy) {
            $data['SortBy'] = $sortBy;
        }

        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.getList',
            ], $data)
        ]);
        return $this->toArray($request);
    }

    public function getContacts()
    {

    }

    public function create(string $domain)
    {
        $data = App::make('config')->get('namecheap.create_domain_request_params');
        if ($this->couponCode) {
            $data['PromotionCode'] = $this->couponCode;
        }
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                "Command" => "namecheap.domains.create",
                'DomainName' => $domain
            ], $data)
        ]);
        return $this->toArray($request);
    }

    public function getTldList()
    {

    }

    public function setContacts()
    {

    }

    public function check(string $domain)
    {
        $domains = [];
        foreach (App::make('config')->get('namecheap.check_tlds') as $tld) {
            $domains[] = $domain . '.' . $tld;
        }
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.check',
                'DomainList' => implode(',', $domains)
            ])
        ]);
        return $this->toArray($request);
    }

    public function reactivate()
    {

    }

    public function renew(string $domain, string $years = '1')
    {
        $data = [
            'Years' => $years,
        ];
        if ($this->couponCode) {
            $data['PromotionCode'] = $this->couponCode;
        }
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.renew',
                'DomainName' => $domain
            ], $data)
        ]);
        return $this->toArray($request);
    }

    public function getRegistrarLock()
    {

    }

    public function getInfo(string $domain)
    {
        $request = $this->client->get("/xml.response", [
            'form_params' => array_merge($this->globalParameters, [
                'Command' => 'namecheap.domains.getInfo',
                'DomainName' => $domain
            ])
        ]);
        return $this->toArray($request);
    }
}
